FROM ubuntu:16.04

RUN apt-get update -y
RUN apt-get install -y python3.4
RUN apt-get install -y python3-pip
RUN mkdir /app
RUN mkdir /app/data/
RUN mkdir /app/datasets/
COPY . /app
WORKDIR /app

COPY requirements.txt ./
RUN pip3 install -U -r requirements.txt

VOLUME ["/data", "/logs", "/datasets"]

EXPOSE 8000
CMD ["gunicorn",  "-b", "0.0.0.0:8000", "-w", "4", "-t", "360", "--log-level", "DEBUG", "run:app"]
