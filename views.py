from __future__ import print_function

from flask import Blueprint, request, Response, current_app, make_response
from datetime import timedelta
from functools import update_wrapper

import json
import re

from flask import Blueprint, request, Response

from application import response_model

bots_app = Blueprint('bots_app', __name__)


def jsonify(payload):
    return Response(json.dumps(
        payload,
        ensure_ascii=False),
        content_type='application/json;charset=UTF-8')


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, str):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, str):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers
            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            h['Access-Control-Allow-Credentials'] = 'true'
            h['Access-Control-Allow-Headers'] = \
                "Origin, X-Requested-With, Content-Type, Accept, Authorization"
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

@bots_app.route('/api/', methods=['GET'])
@crossdomain(origin='*')
def gen():
    context = request.args.get('q')
    max_words = request.args.get('max', None)
    num_words = request.args.get('num', None)
    context = re.sub(u'<.*>', ' ', context)
    split_context = context.lower().strip().split(' ')

    if num_words is not None:
        ans = []
        for _ in range(max_words):
            text = response_model.gen_sentence(split_context)
            if max_words is not None:
                while len(text.split(' ')) > max_words:
                    text = response_model.gen_sentence(split_context)
            ans.append(text)
        return jsonify({'answer': ans})

    else:
        text = response_model.gen_sentence(split_context)
        if max_words is not None:
            while len(text.split(' ')) > max_words:
                text = response_model.gen_sentence(split_context)
        return jsonify({'answer': text})

        # except Exception as e:
        #    raise
        #    return jsonify(['error'])
