# 2ch bot by ODS community #

Repo for AI/BigData/ML/DS/Neural networks/Deep Learning/Statistics/Math/Python/GiveMeMony chat bot

### How to run training ###

1. Put files with data in 
```datasets/ ``` folder
2. Run ```python3 train.py```


### How to run web serving part ###

1. Run ```python3 run.py```

### How to run in container ###

1. ```docker``` and ```docker-compose``` required.
2. Also it's required to have external network ```infranginx_default```. To work without it just edit ```docker-compose.yml``` file
3. Clone repo.
4. Add data to ```datasets``` folder if you want training or trained model to ```data```
5. ```docker-compose build .```
6. ```docker-compose up -d```