# coding=utf-8
from __future__ import unicode_literals, print_function

import gc
import pickle
import re
from collections import defaultdict, Counter

from .utils import get_size

try:
    from sys import intern
except:
    pass

from numpy import random

from .fcounter_compressed import FCounter

end_of_sen = '.!?'


def tokenize(line, strip=False):
    words = re.findall(u'([0-9a-zA-Zа-яА-Яё@\.,!\?:_/\-\']+)', line.lower().strip(end_of_sen))
    if strip:
        words = [w.rstrip(end_of_sen) for w in words]
    return words


def file_reaper(file_name, max_lines=None):
    with open(file_name, 'r', encoding='utf8') as texts:
        for i, line in enumerate(texts):
            if max_lines is not None and i >= max_lines:
                break
            new_sentence = True
            for word in tokenize(line):
                yield intern(word.rstrip(end_of_sen).lower()), new_sentence
                new_sentence = word[-1] in end_of_sen


def handler(obj):
    for item in obj.__dict__:
        yield obj.__getattribute__(item)


class Raver:

    def __init__(self, file_names, context_probability=1,
                 context_expiring=True, word_freq_threshold=5, use_bigrams=False):
        self.context_probability = context_probability
        self.context_expiring = context_expiring
        self.word_freq_threshold = word_freq_threshold
        self.use_bigrams = use_bigrams
        if file_names is None:
            return
        if type(file_names) == str:
            file_names = [file_names]
        fl = []
        for f in file_names:
            if type(f) == str:
                fl.append([f, True, None])
            else:
                fl.append(f)

        self.bigrams = defaultdict(FCounter)
        self.trigrams = defaultdict(FCounter)
        self.first = FCounter()
        self.last_pair = set()  # let's try it without probabilities
        word_count = Counter()
        for filename, use_as_first, max_lines in file_names:
            if max_lines is not None:
                print('Only %d lines' % max_lines)

            p, pp, ppp = None, None, None
            for word, new_sen in file_reaper(filename, max_lines):
                # word = self.coder.encode(word)
                if self.word_freq_threshold > 0:
                    word_count.update((word,))
                if new_sen:
                    self.first.add(word)  # mark as first in sentance
                    if p is not None and pp is not None:
                        self.last_pair.add((ppp, pp, p))  # mark previous as last in sentance
                    p, pp = None, None  # reset previous
                else:
                    self.bigrams[p].add(word)  # update bigrams
                    if pp is not None:
                        self.trigrams[(pp, p)].add(word)  # update trigrams

                ppp, pp, p = pp, p, word

        self.bigrams = dict(self.bigrams)
        self.trigrams = dict(self.trigrams)
        self.first.close()
        trash = {word for word, count in word_count.items() if count <= self.word_freq_threshold}
        trash.add('')
        for key in list(self.bigrams.keys()):
            if key in trash:
                del self.bigrams[key]
            else:
                if not self.bigrams[key].remove_all(trash):
                    del self.bigrams[key]

        for key in list(self.trigrams.keys()):
            f, s = key
            if f in trash or s in trash:
                del self.trigrams[key]
            else:
                if not self.trigrams[key].remove_all(trash):
                    del self.trigrams[key]

        self.first.remove_all(trash)

        for fc in self.bigrams.values():
            fc.close()
        for fc in self.trigrams.values():
            fc.close()

        gc.collect()
        print('BI', len(self.bigrams), 'TRI', len(self.trigrams))

    def _gen_word(self, first, second, context):
        pair = (first, second)
        if pair in self.trigrams:
            return self._context_choice(self.trigrams[(first, second)], context)
        if self.use_bigrams and second in self.bigrams:
            return self._context_choice(self.bigrams[second], context)

    def _context_choice(self, fcount, context, mult=1.):
        if context is not None:
            random.shuffle(context)
            for word in context:
                if word in fcount and random.uniform(0, 1) < self.context_probability * mult:
                    if self.context_expiring:
                        context.remove(word)
                    return word
        return fcount.choice()

    def _gen_first_second(self, context):
        first, second = None, None
        for _ in range(10):
            first = self._context_choice(self.first, context, .5)
            if first not in self.bigrams:
                continue
            second = self._context_choice(self.bigrams[first], context)
            break
        if second is None:
            raise Exception('((((')

        return first, second

    def describe_size(self):
        handlers = {Raver: handler, FCounter: handler}
        for k, v in {'First': self.first, 'Last': self.last_pair,
                     'Bigrams': self.bigrams, 'Trigrams': self.trigrams, 'Total': self}.items():
            if isinstance(v, dict):
                print(k, 'keys', get_size(list(v.keys()), in_mb=True, handlers=handlers), 'mb')
                print(k, 'values', get_size(list(v.values()), in_mb=True, handlers=handlers), 'mb')
                print(k, 'total', get_size(v, in_mb=True, handlers=handlers), 'mb')
            else:
                print(k, get_size(v, in_mb=True, handlers=handlers), 'mb')

    def gen_sentence(self, context=None, as_string=True, apx_length=20.):
        if context is None:
            context = []
        f, s = self._gen_first_second(context)
        sentence = [f, s]
        z = None
        for x in range(150):
            t = self._gen_word(f, s, context)
            if t is None:
                s = self._gen_word(z, f, context)
                continue
            sentence.append(t)
            if (f, s, t) in self.last_pair and random.uniform(0, 1.) < x / apx_length:
                # sentence.append('|')
                break
            z, f, s = f, s, t

        if as_string:
            return ' '.join(sentence)
        else:
            return sentence

    def save(self, fname='rave'):
        pickle.dump(self, open(fname, 'wb'))

    @staticmethod
    def load(fname):
        return pickle.load(open(fname, 'rb'))
