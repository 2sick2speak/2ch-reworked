import sys
from collections import deque
from itertools import chain

from numpy import ndarray


def _get_size(obj, seen=None):
    """Recursively finds size of objects"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle
    # self-referential objects
    seen.add(obj_id)
    if isinstance(obj, ndarray):
        size += obj.nbytes
    elif isinstance(obj, dict):
        size += sum((_get_size(v, seen) for v in obj.values()))
        size += sum((_get_size(k, seen) for k in obj.keys()))
    elif hasattr(obj, '__dict__'):
        size += _get_size(obj.__dict__, seen)
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum((_get_size(i, seen) for i in obj))
    return size


def _get_size2(o, handlers):
    all_handlers = {tuple: iter,
                    list: iter,
                    deque: iter,
                    dict: lambda d: list(chain.from_iterable(d.items())),
                    set: iter,
                    frozenset: iter,
                    }
    all_handlers.update(handlers)  # user handlers take precedence
    seen = set()  # track which object id's have already been seen
    # estimate sizeof object without __sizeof__
    default_size = sys.getsizeof(0)

    def sizeof(o):
        if id(o) in seen:  # do not double count the same object
            return 0
        seen.add(id(o))
        s = sys.getsizeof(o, default_size)
        verbose = False
        if verbose:
            print(s, type(o), repr(o))
        for typ, handler in all_handlers.items():
            if isinstance(o, typ):
                s += sum(map(sizeof, handler(o)))
                break
        # if type(o) in handlers:
        #     s += sum(map(sizeof(0), handlers[type(o)](o)))
        return s

    return sizeof(o)


def get_size(obj, in_mb=False, handlers={}):
    size = _get_size2(obj, handlers)
    # size = _get_size(obj)
    return size if not in_mb else size / (1024. ** 2)
