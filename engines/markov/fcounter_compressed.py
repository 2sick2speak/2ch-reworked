import pickle
import zlib
from collections import defaultdict

from numpy import random


class FCounter:
    ZLIB_THRESHOLD = 7

    def __init__(self, use_zlib='auto'):
        self.data = defaultdict(int)
        self.closed = False
        self.total = 0
        self.use_zlib = use_zlib

    def add(self, word):
        self.data[word] += 1

    def close(self, count_total=True):
        if self.closed:
            raise Exception('Cannot close the Closed')
        if count_total:
            self.total = sum(self.data.values())
        if self.use_zlib is 'auto':
            self.zlib = len(self.data) > FCounter.ZLIB_THRESHOLD
        else:
            self.zlib = self.use_zlib

        if self.zlib:
            self.data = zlib.compress(pickle.dumps(self.data))
        else:
            self.data = pickle.dumps(self.data)
        self.closed = True

    def open(self):
        if not self.closed:
            raise Exception('Cannot open the Open')
        if self.zlib:
            self.data = pickle.loads(zlib.decompress(self.data))
        else:
            self.data = pickle.loads(self.data)
        self.closed = False

    def choice(self):
        closed = self.closed
        if closed:
            self.open()
        # print(list(self.data.keys()), list(map(lambda x: float(x) / self.total, self.data.values())))
        choice = random.choice(list(self.data.keys()), p=list(map(lambda x: float(x) / self.total, self.data.values())))
        if closed:
            self.close(False)
        return choice

    def __contains__(self, item):
        closed = self.closed
        if closed:
            self.open()
        res = item in self.data

        if closed:
            self.close(False)
        return res

    def remove_all(self, trash):
        closed = self.closed
        if closed:
            self.open()
        self.data = {k: v for k, v in self.data.items() if k not in trash}
        res = len(self.data) > 0
        if closed:
            self.close()
        return res
