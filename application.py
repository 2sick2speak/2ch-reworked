from flask import Flask
from settings import PATH
from engines.markov.raver import Raver

# Model loading
response_model = Raver.load(PATH)


def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)

    # import blueprints
    from views import bots_app

    # register blueprints
    app.register_blueprint(bots_app)

    return app