# coding=utf-8
from __future__ import print_function

from engines.markov.raver import Raver
from settings import PATH

def train_model():
    model = Raver([('datasets/2ch.txt', True, 50000),
               ('datasets/pgm.txt', True, 10000),
               ('datasets/vanilla.txt', True, 50000),
               ('datasets/porno.txt', True, 2000)
               ], use_bigrams=True)
    model.save(PATH)

if __name__ == '__main__':
    train_model()
